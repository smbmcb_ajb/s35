const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config()

const app = express();
const port = 3001;

// MongoDB Connection
mongoose.connect(`mongodb+srv://smbmcb:${process.env.MONGODB_PASSWORD}@cluster0.bqemrei.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))
// MongoDB Connection END

app.use(express.json());
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const userSchema = new mongoose.Schema({
	username: String,
	password: String,		
})
// MongoDB Schemas END

// MongoDB Model
const User = mongoose.model('User', userSchema)
// MongoDB Model END

// Routes
app.post('/signup', (request, response) => {
	if(request.body.username == "" || request.body.username == null || request.body.password == "" || request.body.password == null){
				return response.send('username or password cannot be empty')
	}
	else

	User.findOne({username: request.body.username}, (error,result) => {
		if(result != null){

			return response.send('Duplicate username found!')
		}

		let newUser = new User({
			username: request.body.username,
			password: request.body.password
		})

		newUser.save((error, savedTask) => {
			if(error){
				return console.error(error)
			}
			else {
				return response.status(200).send('New User registered!')
			}
		})
	})
})

app.get('/users', (request, response) => {
	User.find({}, (error, result) => {
		if(error){
			return console.log(error)
		}
		let i = 1
		// let data=[];
		// data = result
		result.map((item) => {
			console.log(i + " " + item.username)
			i++
		})
		return response.status(200).json({
			data: result
		})
		
	})
})

// Routes END

app.listen(port, () => console.log(`Server running at localhost:${port}`))